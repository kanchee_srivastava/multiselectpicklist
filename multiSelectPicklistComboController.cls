public with sharing class multiSelectPicklistComboController {
    @AuraEnabled
    public static List<SelectOptionWrapper> getAccountValues(){
       List<SelectOptionWrapper> lstWrapper = new  List<SelectOptionWrapper>();
        try {
            List<Account> accList = [SELECT Id, Name FROM Account WITH SECURITY_ENFORCED ORDER BY Name ASC LIMIT 50000];
            for(Account acc :accList)  {
                lstwrapper.add(new SelectOptionWrapper(acc.Id, acc.Name));
            }   
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return lstwrapper; 
    }
   public class SelectOptionWrapper {
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        public SelectOptionWrapper(string value, string label) {
            this.value = value;
            this.label = label;
        }
    }
}
