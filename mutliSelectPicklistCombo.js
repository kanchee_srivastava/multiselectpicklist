import { LightningElement, track, api } from 'lwc';
import getAccountValues from '@salesforce/apex/multiSelectPicklistComboController.getAccountValues';

export default class MutliSelectPicklistCombo extends LightningElement {

    @api
    values = [];

    @track
    selectedvalues = [];

    @api
    picklistlabel = 'Status';

    showdropdown;

    handleleave() {
        let dropDownCheck= this.showdropdown;
        if(dropDownCheck){
            this.showdropdown = false;
            this.fetchSelectedValues();
        }
    }

    connectedCallback(){
        getAccountValues({
        }).then(resp => {
            if (resp) {
                this.values = resp;
            }
        }).catch(err => {
            console.error(err);
        });
    }
            
    fetchSelectedValues() {
        this.selectedvalues = [];
        //get all the selected values
        this.template.querySelectorAll('c-picklist-value').forEach(
            element => {
               if(element.selected){
                    this.selectedvalues.push(element.label);
                }
            }
        );
        this.refreshOrginalList();
    }

    refreshOrginalList() {
        const picklistvalues = this.values.map(eachvalue => ({...eachvalue}));
        picklistvalues.forEach((element, index) => {
            if(this.selectedvalues.includes(element.value)){
                picklistvalues[index].selected = true;
            }else{
                picklistvalues[index].selected = false;
            }
           
        });
        this.values = picklistvalues;
    
        const custEvent = new CustomEvent( 'getallselectedvalue', {
            detail: JSON.stringify(this.selectedvalues)
        });
        this.dispatchEvent(custEvent);
    }

    handleShowdropdown(){
        let dropDownCheck = this.showdropdown;
        if(dropDownCheck){
            this.showdropdown = false;
            this.fetchSelectedValues();
        }else{
            this.showdropdown = true;
        }
    }

    closePill(event){
        let selection = event.target.dataset.value;
        let selectedpills = this.selectedvalues;
        let pillIndex = selectedpills.indexOf(selection);
        this.selectedvalues.splice(pillIndex, 1);
        this.refreshOrginalList();
    }

    get selectedmessage() {
        if (this.selectedvalues.length > 0 ){
            return this.selectedvalues.length + ' values are selected';
        } else {
            return '';   
        }
    }

}